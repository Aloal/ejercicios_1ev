package unidad3;

import java.util.Scanner;

public class TrianguloFloyd {

	public static void main(String[] args) {
		
		int numero;
							
		Scanner in=new Scanner(System.in);
		
		System.out.println("Dime un numero de filas");
		numero=in.nextInt();
		
		if(numero==1) {
			System.out.println("1");
		}
		if(numero==2) {
            System.out.println("1\n" + "2 3");
	}
        if(numero==3) {
        	System.out.println("1\n" + "2 3\n" + "4 5 6");
        }
        if(numero==4) {
        	System.out.println("1\n" + "2 3\n" + "4 5 6\n" + "7 8 9 10");
        }
}
}
