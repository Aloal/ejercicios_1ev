package unidad4;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
	  ArrayList<String> nombres=new ArrayList();
	  leerPersonas(nombres);
	  System.out.println("Cadena de mayor longitud:" + cadenaMasLarga(nombres));
	   
	}
	  
	public static void leerPersonas(ArrayList<String> nombres) {
		  Scanner in=new Scanner(System.in);
		   String n;
		    boolean masPersonas;
		do {   
		   masPersonas=true;
		   System.out.println("introduce nombre de una persona (fin para terminar");
		   n=in.nextLine();
		   if(n.equalsIgnoreCase("fin")) {   //compara la cadena de texto con el objeto. devuelve true si son iguales
		   masPersonas=false;
		   }else {
			   nombres.add(n);
		   }
		
		} while(masPersonas);
			   
	} 
		
	
	
	 public static String cadenaMasLarga(ArrayList<String> nombres) {
		  String mayor=nombres.get(0);
		  for(int i= 1;i<nombres.size();i++) {
			  if(nombres.get(i).length()>mayor.length()) {
				  mayor=nombres.get(i);
			  }
		  }
        return mayor;

} 
}
