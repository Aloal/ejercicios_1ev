package unidad4;

import java.util.Scanner;

public class Ejercicio5string {

	public static void main(String[] args) {
		Scanner in=new Scanner (System.in);
		String letras="TRWAGMYFPDXBNJZSQVHLCKE";
		
		  
		 System.out.println("Introduce tu nif");
		 String nif=in.nextLine();
		 
		 int numero=Integer.parseInt(nif.substring(0, nif.length()-1));    // cogemos solo el numero
		 char letra=nif.charAt(nif.length()-1);
		 int i=numero%23;                              // resto que corresponde con una letra
		 
		 if(letra==letras.charAt(i)) 
			 System.out.println("El nif es correcto");
			 
		 else 
			 System.out.println("El nif es incorrecto");
			 

	}

}
